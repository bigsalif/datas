<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220516144652 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pienture (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, nom VARCHAR(100) DEFAULT NULL, largeur NUMERIC(6, 2) DEFAULT NULL, hauteur NUMERIC(6, 2) DEFAULT NULL, prix NUMERIC(10, 2) DEFAULT NULL, date_realisation DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, description LONGTEXT DEFAULT NULL, in_portfolio TINYINT(1) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, en_vente TINYINT(1) DEFAULT NULL, INDEX IDX_3FD103D9A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pienture_categorie (pienture_id INT NOT NULL, categorie_id INT NOT NULL, INDEX IDX_AF44C563EE07A31 (pienture_id), INDEX IDX_AF44C56BCF5E72D (categorie_id), PRIMARY KEY(pienture_id, categorie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pienture ADD CONSTRAINT FK_3FD103D9A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE pienture_categorie ADD CONSTRAINT FK_AF44C563EE07A31 FOREIGN KEY (pienture_id) REFERENCES pienture (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pienture_categorie ADD CONSTRAINT FK_AF44C56BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pienture_categorie DROP FOREIGN KEY FK_AF44C563EE07A31');
        $this->addSql('DROP TABLE pienture');
        $this->addSql('DROP TABLE pienture_categorie');
    }
}
