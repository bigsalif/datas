<?php

namespace App\Tests;

use DateTime;
use App\Entity\User;
use App\Entity\Pienture;
use App\Entity\Categorie;
use PHPUnit\Framework\TestCase;

class PeintureUnitTest extends TestCase
{
    public function testIsTrue():void
    {
       
        $categorie= new Categorie();
        $peinture= new Pienture();
        $datetime= new DateTime();
        $user=new User();

        $peinture->setNom('nom')
        ->setLargeur(20.20)
        ->setHauteur(20.20)
        ->setEnvente(true)
        ->setDateRealisation($datetime)
        ->setCreatedAt($datetime)
        ->setDescription("description")
        ->setInPortfolio(true)
        ->setSlug("slug")
        ->setFile("file")
        ->setPrix(20.20)
        ->addCategorie($categorie)
        ->setUser($user);
        $this->assertTrue($peinture->getNom() === 'nom');
        $this->assertTrue($peinture->getLargeur() == 20.20);
        $this->assertTrue($peinture->getHauteur() == 20.20);
        $this->assertTrue($peinture->isEnVente() === true);
        $this->assertTrue($peinture->getDateRealisation() === $datetime);
        $this->assertTrue($peinture->getCreatedAt() ===$datetime);
        $this->assertTrue($peinture->getDescription() === 'description');
        $this->assertTrue($peinture->isInPortfolio() === true);
        $this->assertTrue($peinture->getSlug() === 'slug');
        $this->assertTrue($peinture->getFile() === 'file');
        $this->assertTrue($peinture->getPrix() == 20.20);
        $this->assertContains($categorie,$peinture->getCategorie());
        $this->assertTrue($peinture->getUser() === $user);
    }

    public function testIsFalse():void
    {
        $categorie= new Categorie();
        $peinture= new Pienture();
        $datetime= new DateTime();
        $user=new User();

        $peinture->setNom('nom')
        ->setLargeur(20.20)
        ->setHauteur(20.20)
        ->setEnvente(true)
        ->setDateRealisation($datetime)
        ->setCreatedAt($datetime)
        ->setDescription("description")
        ->setInPortfolio(true)
        ->setSlug("slug")
        ->setFile("file")
        ->setPrix(20.20)
        ->addCategorie($categorie)
        ->setUser($user);

        $this->assertFalse($peinture->getNom() === 'false');
        $this->assertFalse($peinture->getLargeur() === 22.20);
        $this->assertFalse($peinture->getHauteur() === 22.20);
        $this->assertFalse($peinture->isEnVente() === false);
        $this->assertFalse($peinture->getDateRealisation() === new DateTime);
        $this->assertFalse($peinture->getCreatedAt() === new DateTime);
        $this->assertFalse($peinture->getDescription() === 'false');
        $this->assertFalse($peinture->isInPortfolio() === false);
        $this->assertFalse($peinture->getSlug() === 'false');
        $this->assertFalse($peinture->getFile() === 'false');
        $this->assertFalse($peinture->getPrix() === 22.20);
        $this->assertNotContains(new Categorie,$peinture->getCategorie());
        $this->assertFalse($peinture->getUser() === new User);
    }
    public function testIsEmpty():void
    {
        $peinture= new Pienture();
        $this->assertEmpty($peinture->getNom());
        $this->assertEmpty($peinture->getLargeur());
        $this->assertEmpty($peinture->getHauteur());
        $this->assertEmpty($peinture->isEnVente());
        $this->assertEmpty($peinture->getDateRealisation());
        $this->assertEmpty($peinture->getCreatedAt());
        $this->assertEmpty($peinture->getDescription());
        $this->assertEmpty($peinture->isInPortfolio());
        $this->assertEmpty($peinture->getSlug());
        $this->assertEmpty($peinture->getFile());
        $this->assertEmpty($peinture->getPrix());
        $this->assertEmpty($peinture->getUser());
        $this->assertEmpty($peinture->getCategorie());
    }
}
