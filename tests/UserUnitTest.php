<?php

    namespace App\Tests;
    
    use App\Entity\User;
    use PHPUnit\Framework\TestCase;
    
    class UserUnitTest extends TestCase
    { /*
        public function testSomething(): void
        {
            $this->assertTrue(true);
        }
        */
        public function testIsTrue():void
        {
            $user= new User();
            $user->setEmail('true@test.com')
            ->setNom('nom')
            ->setPrenom('prenom')
            ->setPassword('password')
            ->setAPropos('a propos')
            ->setWhatsapp('whastapp');
    
            $this->assertTrue($user->getEmail() === 'true@test.com');
            $this->assertTrue($user->getNom() === 'nom');
            $this->assertTrue($user->getPrenom() === 'prenom');
            $this->assertTrue($user->getPassword() === 'password');
            $this->assertTrue($user->getAPropos() === 'a propos');
            $this->assertTrue($user->getWhatsapp() === 'whastapp');
    }

    public function testIsFalse():void
    {
        $user= new User();
        $user->setEmail('true@test.com')
        ->setNom('nom')
        ->setPrenom('prenom')
        ->setPassword('password')
        ->setAPropos('a propos')
        ->setWhatsapp('whastapp');

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAPropos() === 'false');
        $this->assertFalse($user->getWhatsapp() === 'false');
    }

    public function testIsEmpty():void
    {
        $user= new User();
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getWhatsapp());
    }

}
